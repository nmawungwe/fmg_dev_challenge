//public/core.js

var app = angular.module('scotchTodo', ["ngRoute"]);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "templates/login.html"
    })
    .when("/home", {
        templateUrl : "templates/home.html"
    })
    .when("/register",{
        templateUrl : "templates/registration.html"
    })
})

function mainController($scope, $http, $window, $rootScope) {
    $scope.firstname = null;
    $scope.surname = null;
    $scope.username = null;
    $scope.password = null;
    $scope.repeatpassword = null;
    $scope.color = null;
    $scope.error = null;

    $scope.login = function() {
        const payload = {username: $scope.username, password: $scope.password};
        if(!$scope.username || !$scope.password){
            $scope.error = "Either username or password is not supplied"
            return;
        }
        console.log("payload", payload);
        $http.post('/api/login', payload)
            .success(function(data) {
                $scope.userInfo = data;
                $rootScope.userInfo = data;
                console.log(data);
                $window.location.href="#/home"
            })
            .error(function(data) {
                console.log('Error: ', data);
                $scope.error = "Either username or password is incorrect"
            });
    };

    $scope.register = function(valid){
        const payload = {firstname: $scopefirstname, surname: $scope.surname, username: $scope.username, password: $scope.password, repeatpassword: $scope.repeatpassword, color: $scope.color};
        if(valid) 

            $http.post('/api/register', payload)
            .success(function(data) {
                $scope.userInfo = data;
                $rootScope.userInfo = data;
                console.log(data);
                $window.location.href="#/home"
                })
                .error(function(data) {
                    console.log('Error: ', data);
        $scope.error = "Registeration details incorrect"
                });
            
};

function homeController($scope, $http, $window, $rootScope) {
    $scope.userInfo = $rootScope.userInfo;
}
    }
