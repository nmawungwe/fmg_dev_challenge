// serve.js

// set up ==========

var express = require('express');
var app = express(); // creating our app w/ express
var mongoose = require('mongoose') //mongoose for mongodb
var morgan = require('morgan'); // loging requests to the console (express 4) (helps with debugging and seeing behind the scenes how app is behaving)
bodyParser = require('body-parser'); // pull information from HTML Post ( express 4)
var methodOverride = require('method-override'); //  simulate DELETE and PUT (express 4)

// configuration
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://nmawungwe:futuremedia@cluster0-shard-00-00-hxozj.mongodb.net:27017,cluster0-shard-00-01-hxozj.mongodb.net:27017,cluster0-shard-00-02-hxozj.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true', { useNewUrlParser: true }); 
mongoose.connection.on('error', function(err) {console.log(err)});
   // connect to mongoDB database on modulus.io

app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users
app.use(morgan('dev'));  // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));   // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());



// define user model =================
var User = mongoose.model('User', {
    firstname : String, 
    surname : String,
    username: String,
    password: String,
    color: String
});

 // server.js
 // routes ======================================================================

 app.use(express.static('public'))


app.post('/api/login', function(req, res) {
    // use mongoose to get all user details in the database
    User.findOne({username: req.body.username, password: req.body.password}, function(err, user) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err)
            return res.send(err)
        if (user)
            return res.json(user); // return all user details in JSON format
        else {
            return res.status(401).send({
                error_messsage: 'Incorrect login details'
            })
        }
    });
});

// create todo and send back all todos after creation
app.post('/api/registration', function(req, res) {

    // create a todo, information comes from AJAX request from Angular
    console.log(req.body);
    User.create({
        firstname : req.body.firstname,
        surname : req.body.surname,
        username : req.body.username,
        password : req.body.password,
        color : req.body.color
    }, function(err, user) {
        if (err)
            res.send(err);
        res.json(user)
    });

});


 // listen (start app with node server.js) ===================

 // application -------------------------------------------------------------
 app.get('*', function(req, res) {
    res.sendFile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

 app.listen(8080);
 console.log("App listening on port 8080");

